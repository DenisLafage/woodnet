# libraries

library(tidyverse)
library(PrimerMiner)
library(bold)
library(plyr)
library(traits)

# Import sp list

sp_list <- read_csv('./csv/sp_list.csv')


# Bold

#bold_COI <- lapply(sp_list$Species, bold_seq,  marker = c("COi", "CO1", "COXi", "COX1"))

bold_spid <- bold_seq(sp_list$Species, marker = c("COi", "CO1", "COXi", "COX1"))
bold_spid_flat <-flatten(bold_spid)
bold_spid_2 <- bold_spid_flat[grepl('name', names(bold_spid_flat))]
bold_spid_COI <- ldply(bold_spid_2) %>% 
  dplyr::count(V1) %>% 
  mutate(name_2 = word(V1, 1,2)) %>% 
  group_by(name_2) %>% 
  dplyr::summarise(n2 = sum(n))


bold_spid_12S <- bold_seq(sp_list$Species, marker = c('12S', '12s'))
bold_spid_16S <- bold_seq(sp_list$Species, marker = c('16S'))
bold_spid_H3 <- bold_seq(sp_list$Species, marker = c('H3'))
bold_spid_18S <- bold_seq(sp_list$Species, marker = c('18S'))
bold_spid_28S <- bold_seq(sp_list$Species, marker = c('28S'))


bold_spid_flat <-flatten(bold_spid)
bold_spid_2 <- bold_spid_flat[grepl('name', names(bold_spid_flat))]
bold_spid_COI <- ldply(bold_spid_2) %>% 
  dplyr::count(V1) %>% 
  mutate(name_2 = word(V1, 1,2)) %>% 
  group_by(name_2) %>% 
  dplyr::summarise(n2 = sum(n))


tab_seq <-sp_list %>% 
  left_join(bold_spid_COI, by = c("Species" = "name_2"))


# NCBI

ncbi_spid <- ncbi_searcher(taxa = "Agelena labyrinthica	", seqrange = "0:1000",getrelated=FALSE, entrez_query = 'COI')
unique(ncbi_spid_1$gene_desc)


ncbi_spid <- lapply (sp_list$Species, ncbi_searcher, seqrange = "0:1000", getrelated=FALSE, entrez_query = 'COI')
